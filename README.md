# E Shop

engines: <br />
node: ">=18.17", <br />
npm: ">=9"

## Backup and restore DB

```bash
$ mongodump --db=eshop-database --uri="mongodb://127.0.0.1:27017"
$ mongorestore --drop dump/

windows
.\mongodump --db=eshop-database --uri="mongodb://127.0.0.1:27017"
.\mongorestore --drop dump/
```

## PostgreSQL

от имени пользователя eshop необходимо создать новую схему

```bash
CREATE SCHEMA eshop;
```
