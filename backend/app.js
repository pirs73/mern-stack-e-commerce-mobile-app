require('dotenv/config');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');
//Routes
const categoriesRoutes = require('./routes/categories');
const productsRoutes = require('./routes/products');
const usersRoutes = require('./routes/users');
const ordersRoutes = require('./routes/orders');
const authJwt = require('./helpers/jwt');
const errorHandler = require('./helpers/error-handler');

const app = express();
const API = process.env.API_URL;
const MONGO_UTI = process.env.MONGO_DB_URL_LOCAL;

app.use(cors());
app.options('*', cors);

/** middleware */
app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use(authJwt());
app.use('/public/uploads', express.static(__dirname + '/public/uploads'));
app.use(errorHandler);
/** Routers */
app.use(`${API}/categories`, categoriesRoutes);
app.use(`${API}/products`, productsRoutes);
app.use(`${API}/users`, usersRoutes);
app.use(`${API}/orders`, ordersRoutes);

mongoose
  .connect(`${MONGO_UTI}`, {
    autoIndex: true,
    maxPoolSize: 10, // Maintain up to 10 socket connections
    serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4, // Use IPv4, skip trying IPv6
  })
  .then(() => {
    console.log('Database connection is ready...');
  })
  .catch((err) => {
    console.log(err);
  });

app.listen(4000, () => {
  console.log('Server is running http://localhost:4000');
});
