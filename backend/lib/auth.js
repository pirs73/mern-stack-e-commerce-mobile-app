/* eslint-disable no-console */
const Iron = require('@hapi/iron');
const { MAX_AGE, setTokenCookie, getTokenCookie } = require('./auth-cookies');

/** min 32 знака */
const TOKEN_SECRET =
  process.env.TOKEN_SECRET ||
  'very_HARD_TOKEN_SECRET_string_reyre_77777778888888_min32_chars';

async function setLoginSession(res, session) {
  const createdAt = Date.now();
  // Create a session object with a max age that we can validate later
  const obj = { ...session, createdAt, maxAge: MAX_AGE };
  const token = await Iron.seal(obj, TOKEN_SECRET, Iron.defaults);
  // setTokenCookie(res, token);
  return token;
}

async function getLoginSession(req) {
  const token = getTokenCookie(req);

  if (!token) return;

  const session = await Iron.unseal(token, TOKEN_SECRET, Iron.defaults);
  const expiresAt = session.createdAt + session.maxAge * 1000;

  // Validate the expiration date of the session
  if (Date.now() > expiresAt) {
    throw new Error('Session expired');
  }

  return { session, token };
}

module.exports = {
  setLoginSession,
  getLoginSession,
};
