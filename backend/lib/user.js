const crypto = require('crypto');
// Compare the password of an already fetched user (using `findUser`) and compare the
// password for a potential match
function validatePassword(user, inputPassword) {
  const inputHash = crypto
    .pbkdf2Sync(inputPassword, user.salt, 1000, 64, 'sha512')
    .toString('hex');

  const passwordsMatch = user.passwordHash === inputHash;

  return passwordsMatch;
}

module.exports = validatePassword;
