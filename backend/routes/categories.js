const { Category } = require('../models/category');
const express = require('express');
const router = express.Router();

router.get(`/`, async (req, res) => {
  const categoryList = await Category.find();

  if (!categoryList) {
    res.status(500).json({ success: false });
  }
  res.status(200).send(categoryList);
});

router.get(`/:id`, async (req, res) => {
  const category = await Category.findById(req.params.id);

  if (!category) {
    res
      .status(404)
      .json({ message: 'The category with the given ID was not found.' });
  }

  res.status(201).send(category);
});

router.post(`/`, async (req, res) => {
  let category = new Category({
    name: req.body.name,
    icon: req.body.icon,
    color: req.body.color,
  });

  category = await category.save();

  if (!category) {
    return res.status(502).send('the category cannot be created');
  }

  res.status(201).send(category);
});

router.put(`/:id`, async (req, res) => {
  const category = await Category.findOneAndUpdate(
    { _id: req.params.id },
    {
      name: req.body.name,
      icon: req.body.icon,
      color: req.body.color,
    },
    {
      new: true,
    },
  );

  if (!category) {
    return res.status(502).send('the category cannot be updated');
  }

  res.status(200).send(category);
});

router.delete(`/:id`, (req, res) => {
  Category.deleteOne({ _id: req.params.id })
    .then((category) => {
      if (category && category.deletedCount) {
        return res.status(200).json({
          success: true,
          message: `the category Id ${req.params.id} is deleted`,
        });
      } else {
        return res.status(404).json({
          success: false,
          message: `category Id ${req.params.id} not found`,
        });
      }
    })
    .catch((error) => {
      return res.status(400).json({ success: false, error });
    });
});

module.exports = router;
