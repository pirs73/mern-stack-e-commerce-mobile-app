import React, { useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import baseUrl from './assets/common/baseUrl';

export default function Home() {
  useEffect(() => {
    const products = async () => {
      try {
        const res = await fetch(`${baseUrl}products`, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        });
        if (res.ok) {
          const data = await res.json();
          //   console.log(data);
          return data;
        } else {
          console.error(res.status);
        }
      } catch (error) {
        console.error(error);
        throw new Error(`Что то пошло не так ${error}`);
      }
    };

    products();
  }, []);
  return (
    <View style={styles.container}>
      <Text>Hello!!!</Text>
      <Text>E Shop</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
