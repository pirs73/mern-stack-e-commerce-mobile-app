import { Platform } from 'react-native';

let baseUrl = '';

{
  Platform.OS === 'android'
    ? (baseUrl = 'http://192.168.1.141:4000/api/v1/')
    : (baseUrl = 'http://localhost:4000/api/v1/');
}

export default baseUrl;
