import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from '@app/app.controller';
import { AppService } from '@app/app.service';
import { CategoryModule } from '@app/category/category.module';
import ormconfig from './ormconfig';

@Module({
  imports: [TypeOrmModule.forRoot(ormconfig), CategoryModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
