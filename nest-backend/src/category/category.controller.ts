import { Controller, Get } from '@nestjs/common';
import { CategoryService } from '@app/category/category.service';

@Controller('api/v1/categories')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}
  @Get()
  getCategories() {
    return this.categoryService.getCategories();
  }
}
