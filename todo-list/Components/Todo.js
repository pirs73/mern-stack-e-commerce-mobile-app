import React from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';

const Todo = ({ item, deleteText }) => {
  return (
    <View style={[styles.item, { margin: 8, padding: 8 }]}>
      <Text>{item}</Text>
      <Button title={'Delete'} color={'red'} onPress={() => deleteText(item)} />
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: 'whitesmoke',
  },
});

export default Todo;
